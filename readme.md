# Remarkable/frontend-test

Module for adding unit tests to your project

## Integration

Install with

```
npm install --save-dev @remarkable/frontend-test
```

Then, add in the following code to your `package.json`

```
{
    "scripts": {
        "test": "nyc mocha-webpack test/**/*.js --mode development",
        "test:w": "mocha-webpack \"test/**/*.js\" --mode development --watch"
    },
    nyc: {
        "extends": "@istanbuljs/nyc-config-babel"
    }
}
```
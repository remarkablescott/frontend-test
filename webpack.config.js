module.exports = {
    output: {
        libraryTarget: 'umd',
        globalObject: 'this'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader", 
                    options: {
                        cacheDirectory: true,
                        presets: [
                            [
                                'babel-preset-env',
                                {
                                    modules: false
                                }
                            ]
                        ],
                        plugins: [
                            "istanbul"
                        ]
                    }
                }
            }
        ]
    }
};